var webpack = require('webpack');
var path = require('path');

var WriteFilePlugin = require('write-file-webpack-plugin');


var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src/app');
var ASSET_DIR = path.resolve(__dirname, 'assets');

var config = {
    entry: {
        app_js: [
            APP_DIR + '/app.jsx'
        ],
    },
    output: {
        path: BUILD_DIR + "/public",
        filename: 'bundle.js',
        chunkFilename: '[id].[chunkhash].js',
        publicPath: '/assets/',
    },
    module: {
        loaders: [
            {
                test: /\.jsx?/,
                include: APP_DIR,
                loaders: ['babel-loader']
            },
            {
                test: /\.css$/,
                loaders: ['style-loader', 'css-loader']
            }
        ]
    },
    plugins: [
        new WriteFilePlugin()
    ]
};

module.exports = config;