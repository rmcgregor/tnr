import React from 'react';
import {render} from 'react-dom';
import * as io from 'socket.io-client';

import Button from './button.jsx';
import Slider from './silder.jsx';

import {Column, Table} from 'react-virtualized';
import 'react-virtualized/styles.css';

class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {cubes: []};
    }

    componentDidMount() {
        var self = this;

        self.connection = new io.connect('http://' + document.domain + ':' + location.port);

        self.connection.on("connect", function () {
            console.log("connected!");
            self.connection.emit("my event", {data: "I'm connected"});
        });
        self.connection.on("model.state", function (event) {
            console.log(event);
            self.setState({cubes: event.cubes.entries});
        });
    }

    move(direction) {
        this.connection.emit("move", {"direction": direction});
    }

    setRotY(y) {
        this.connection.emit("rot", {"y": y});
    }

    render() {
        const list = this.state.cubes;

        return (
          <Table
            width={600}
            height={1000}
            headerHeight={20}
            rowHeight={30}
            rowCount={list.length}
            rowGetter={({index}) => list[index]}
          >
            <Column
              label='Ident'
              dataKey='key'
              width={100}
            />
            <Column
              width={200}
              label='position'
              dataKey='pos'
              cellDataGetter={({ columnData, dataKey, rowData }) => `${rowData.value[dataKey].x}, ${rowData.value[dataKey].y}, ${rowData.value[dataKey].z}` }
            />
            <Column
              width={200}
              label='rotation'
              dataKey='rot'
              cellDataGetter={({ columnData, dataKey, rowData }) => JSON.stringify(rowData.value[dataKey])}
            />
            </Table>
        );
    }
}

render(<App/>, document.getElementById('app'));