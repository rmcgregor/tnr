import React from 'react';

class Slider extends React.Component {
    constructor(props) {
        super(props);
    }

    onChange (e) {
        this.props.onChange(e.target.value);
    }

    render () {
        return (
            <input type="range" min="0" max="3" value="0" step="0.1" className="slider" 
            id={this.props.name} onChange={(e) => this.onChange(e)}/>
        );
    }
}

export default Slider;