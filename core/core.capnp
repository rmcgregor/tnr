@0x96a58bfa93be9247;

annotation topic(struct) :Text;
annotation service(interface) :Text;

struct Topic {
    name @0 :Text;
    address @1 :Text;
}

struct Service {
    name @0 :Text;
    address @1 : Text;
}

struct Discover {
}

struct DiscoverResponse {
    topics @0 :List(Topic);
    services @1 :List(Service);
}

interface Core {
    topics @0 () -> (topics: List(Topic));
    services @1 () -> (services: List(Service));
}