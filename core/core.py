import zmq
import zmq.asyncio
from zmq.devices import ThreadProxy

import logging
import time
import threading


PUB_PORT = 5560
SUB_PORT = 5559
MON_PORT = 5558

logger = logging.getLogger(__name__)


class Forwarder(object):
    def __init__(self):
        self.proxy = None

    def open(self):
        self.proxy = ThreadProxy(zmq.XSUB, zmq.XPUB, zmq.PUB)
        self.proxy.daemon = True
        self.proxy.bind_in("tcp://*:{}".format(SUB_PORT))
        self.proxy.bind_out("tcp://*:{}".format(PUB_PORT))
        self.proxy.bind_mon("tcp://*:{}".format(MON_PORT))
        self.proxy.start()

    def close(self):
        self.proxy.context_factory().term()
        self.proxy.join()

    def __enter__(self):
        self.open()
        return self

    def __exit__(self, *args):
        self.close()


class Core(object):
    def __init__(self):
        self.topic_forwarder = Forwarder()

    def open(self):
        self.topic_forwarder.open()
        self._listener = threading.Thread(target=self.monitor)
        self._listener.daemon = True
        self._listener.start()

    def close(self):
        self.topic_forwarder.close()

    def __enter__(self):
        self.open()
        return self

    def monitor(self):
        ctx = zmq.Context()
        sock = ctx.socket(zmq.SUB)
        sock.connect("tcp://localhost:{}".format(PUB_PORT))
        sock.setsockopt(zmq.SUBSCRIBE, b"")
        while True:
            topic, msg = sock.recv_multipart()
            logging.info("<<<[{}]{!r}".format(topic.decode(), msg))

    def __exit__(self, *args):
        self.close()


def main():
    logging.basicConfig(level=logging.DEBUG)
    with Core() as c:
        while True:
            time.sleep(0.1)


if __name__ == '__main__':
    main()
