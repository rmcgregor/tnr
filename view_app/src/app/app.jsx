import React from 'react';
import React3 from 'react-three-renderer';
import * as THREE from 'three';
import ReactDOM from 'react-dom';
import * as io from 'socket.io-client';

import Cube from './cube.jsx';

var OrbitControls = require('three-orbit-controls')(THREE);

class Simple extends React.Component {
  constructor(props, context) {
    super(props, context);

    // construct the position vector here, because if we use 'new' within render,
    // React will think that things have changed when they have not.
    this.cameraPosition = new THREE.Vector3(0, 0, 5);

    this.state = {primatives: []};

    this._onAnimate = () => {
      // we will get this callback every frame
    };
  }

  componentDidMount() {
    var self = this;

    const controls = new OrbitControls(this.refs.camera);
    this.controls = controls;

    self.connection = new io.connect('http://' + document.domain + ':' + location.port);

    self.connection.on("connect", function () {
      console.log("connected!");
    });
    self.connection.on("model.state", function (event) {
      var primatives = [];
      for (var e of event.cubes.entries) {
        console.log(e.value);
        var entry = {
          pos: new THREE.Vector3(),
          rot: new THREE.Vector3(),
          color: 0x00ff00
        };
        primatives.push(entry);
      }
      console.log(primatives)
      self.setState({ primatives: primatives });
    });
  }

  render() {
    const width = window.innerWidth; // canvas width
    const height = window.innerHeight; // canvas height

    const cubeMeshes = this.state.primatives.map(({ pos, rot, color }, i) =>
      (<Cube
        key={i}

        position={pos}
        rotation={rot}
        color={color}
      />));

    return (<React3
      mainCamera="camera" // this points to the perspectiveCamera which has the name set to "camera" below
      width={width}
      height={height}

      onAnimate={this._onAnimate}
    >
      <scene>
        <perspectiveCamera
          name="camera"
          ref="camera"
          fov={75}
          aspect={width / height}
          near={0.1}
          far={1000}

          position={this.cameraPosition}
        />
        {cubeMeshes}
      </scene>
    </React3>);
  }
}

ReactDOM.render(<Simple/>, document.body);