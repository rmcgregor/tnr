import React from 'react';
import React3 from 'react-three-renderer';
import * as THREE from 'three';

class Cube extends React.Component {
    constructor (props) {
        super(props);
    }

    static defaultProps = {
        color: 0x00ff00,
        position: THREE.Vector3(0, 0, 0),
        rotation: THREE.Euler(0, 0, 0)
    }

    render () {
        return (
            <mesh
                rotation={this.props.rotation}
                position={this.props.position}
            >
                <boxGeometry
                    width={1}
                    height={1}
                    depth={1}
                />
                <meshBasicMaterial
                    color={this.props.color}
                />
            </mesh>
        )
    }
}

export default Cube;