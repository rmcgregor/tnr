/* eslint-disable import/no-extraneous-dependencies */
var webpack = require('webpack');
var path = require('path');
var WriteFilePlugin = require('write-file-webpack-plugin');

var BUILD_DIR = path.resolve(__dirname, 'build');
var APP_DIR = path.resolve(__dirname, 'src/app');

const babelLoaderConfigShared = {
  test: /\.jsx?$/,
  loader: 'babel-loader',
  query: {
    presets: [
      "es2015",
      "stage-0",
      "react"
    ],
    plugins: [
      "transform-runtime",
      "transform-decorators-legacy"
    ],
    cacheDirectory: true,
  },
};

const config = {
  entry: APP_DIR + '/app.jsx',
  output: {
    path: BUILD_DIR + '/public',
    filename: 'bundle.js'
  },
  module: {
    loaders: [
        {
          exclude: /node_modules/,
          ...babelLoaderConfigShared
        },
        {
          include: /react-three-renderer[\\/]src/,
          ...babelLoaderConfigShared
        },
    ],
  },
  resolve: {
    extensions: ['.js', '.jsx'],
      alias: {
        // use the source files
        'react-three-renderer': path.join(
        __dirname, 'node_modules', 'react-three-renderer', 'src'),
    },
  },
  devServer: {
    contentBase: path.join(__dirname, 'assets'),
    // noInfo: true, //  --no-info option
    hot: true,
    inline: true,
    stats: { colors: true },
  },
  plugins: [
      new WriteFilePlugin()
  ]
};


module.exports = config;