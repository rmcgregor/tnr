import asyncio
import os

import capnp
import socketio
import zmq
import zmq.asyncio
from aiohttp import web

here = os.path.abspath(os.path.dirname(__file__))

capnp.remove_import_hook()
model_capnp = capnp.load(os.path.join(here, "..", "model", "model.capnp"))

TOPIC_PORT = 5560

sio = socketio.AsyncServer()
app = web.Application()
sio.attach(app)


async def zmq_to_websock(app):
    try:
        ctx = zmq.asyncio.Context()
        sock = ctx.socket(zmq.SUB)
        sock.connect("tcp://localhost:{}".format(TOPIC_PORT))
        sock.subscribe(b"model.state")
        while True:
            topic, msg = await sock.recv_multipart()
            model = model_capnp.Model.State.from_bytes_packed(msg)
            model = model.to_dict()
            await sio.emit(topic.decode(), model)
    except asyncio.CancelledError:
        pass
    finally:
        sock.close()
        ctx.term()


async def index(request):
    with open(os.path.join(here, 'src', 'index.html')) as f:
        return web.Response(text=f.read(), content_type="text/html")


@sio.on('connect')
def connect(sid, environ):
    print("connect ", sid)


async def start_background_tasks(app):
    app['zmq_forwarder'] = app.loop.create_task(zmq_to_websock(app))


async def cleanup_background_tasks(app):
    app['zmq_forwarder'].cancel()
    await app['zmq_forwarder']


app.router.add_static("/assets", os.path.join(here, "build", "public"))
app.router.add_get("/", index)

if __name__ == '__main__':
    app.on_startup.append(start_background_tasks)
    app.on_cleanup.append(cleanup_background_tasks)
    web.run_app(app)
