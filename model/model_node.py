from typing import NamedTuple

import capnp
import zmq
import time
import threading

import model_capnp


class Vec3(NamedTuple):
    x: float
    y: float
    z: float

    def to_dict(self):
        return dict(self._asdict())


class Cube(NamedTuple):
    pos: Vec3
    rot: Vec3

    def to_dict(self):
        return {"pos": self.pos.to_dict(), "rot": self.rot.to_dict()}


class Model(object):
    def __init__(self):
        self.cubes = {"9889": Cube(Vec3(1, 2, 0), Vec3(0, 0.1, 0))}

    def open(self):
        self._context = zmq.Context()
        self._pub = self._context.socket(zmq.PUB)
        self._pub.connect("tcp://localhost:5559")

        self._pub_t = threading.Thread(target=self._publisher, daemon=True)
        self._pub_t.start()

    def close(self):
        self._pub.close()
        self._context.term()

    def add_cube(self, cube):
        ident = str(id(cube))
        self.cubes[ident] = cube
        return ident

    def remove_cube(self, ident):
        del self.cubes[ident]

    def __enter__(self):
        self.open()


    def __exit__(self, *args):
        self.close()

    def _publish(self):
        msg = model_capnp.Model.State.new_message()
        entries = msg.cubes.init_resizable_list("entries")
        for key, cube in self.cubes.items():
            entry = entries.add()
            entry.key = key
            entry.value.from_dict(cube.to_dict())
        entries.finish()
        self._pub.send_multipart([b"model.state", msg.to_bytes_packed()])

    def _publisher(self):
        while not self._context.closed:
            self._publish()
            time.sleep(2)


class ModelServer(model_capnp.Model.Server):
    def __init__(self, model):
        self.model = model

    def addCube(self, cube, ctx):
        cube = Cube(**cube.to_dict())
        return self.model.add_cube(cube)

    def removeCube(self, ident, ctx):
        self.model.remove_cube(ident)


def main():
    with Model() as model:
        server = capnp.TwoPartyServer(
            "localhost:6666", bootstrap=ModelServer(model))
        server.run_forever()


if __name__ == '__main__':
    main()