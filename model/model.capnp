@0xf213114f5f89dbd9;

struct Vec3 {
    x @0 :Float32;
    y @1 :Float32;
    z @2 :Float32;
}

struct Cube {
    pos @0 :Vec3;
    rot @1 :Vec3;
}

struct Map(Key, Value) {
    entries @0 :List(Entry);
    struct Entry {
        key @0 :Key;
        value @1 :Value;
    }
}

interface Model {
    struct State {
        cubes @0 :Map(Text, Cube);
    }

    addCube @0 (cube: Cube) -> (ident: Text);
    removeCube @1 (ident: Text);
}